#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail

source ./proxmox_credentials.sh

TICKET=$(curl -ksS -d "username=${PROXMOX_USERNAME}" --data-urlencode "password=${PROXMOX_PASSWORD}"  "${PROXMOX_URL}api2/json/access/ticket")
echo $TICKET | jq --sort-keys '.' || echo $TICKET
echo $TICKET | jq --raw-output '.data.ticket' > /tmp/proxmox_tmp_ticket.txt
echo $TICKET | jq --raw-output '.data.CSRFPreventionToken' > /tmp/proxmox_tmp_csrftoken.txt
chmod 600 /tmp/proxmox_tmp_ticket.txt /tmp/proxmox_tmp_csrftoken.txt
