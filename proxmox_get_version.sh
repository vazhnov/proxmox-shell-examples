#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail

source ./proxmox_ticket_update.sh

VERSION=$(curl --silent --insecure  --cookie "PVEAuthCookie=$(</tmp/proxmox_tmp_ticket.txt)" --header "CSRFPreventionToken:$(</tmp/proxmox_tmp_csrftoken.txt)" "${PROXMOX_URL}api2/json/version")
echo $VERSION | jq --sort-keys '.'
