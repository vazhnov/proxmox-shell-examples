# Proxmox shell examples

## How to use

Copy `proxmox_credentials.sh.example` to `proxmox_credentials.sh`, fill it.

Run `proxmox_ticket_get.sh` to get toker for 2 hours.

Run `proxmox_get_version.sh` to check API works.

## Sources

* [gitlab](https://gitlab.com/vazhnov/proxmox-shell-examples) — this repository

## Useful links

* [Proxmox VE API](https://pve.proxmox.com/wiki/Proxmox_VE_API) — official documentation
* [How to get new ticket?](https://forum.proxmox.com/threads/api-how-to-get-new-ticket.19034/) — use `--data-urlencode`
* [ProxBash](https://raymii.org/s/software/ProxBash.html) — wrapper under `pvesh` ([source](https://github.com/RaymiiOrg/raymiiorg.github.io/blob/master/inc/software/proxbash.sh)?)
* [Proxmox\_apt\_upgrade\_all\_pct.sh](https://gitlab.com/snippets/1891816) — upgrade all debian-based LXC environments in Proxmox

## Copyright

Distrubuted under MIT license.

