#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail

# Do upgrade for all debian-based LXC environments in Proxmox.
#
# Copyright:
# Distributed under MIT license or newer.
#
# Source:
# https://gitlab.com/vazhnov/proxmox-shell-examples

for ContainerID in $(sudo pct list | grep '\brunning\b' | awk '/[0-9]/{print $1}')
do
  echo "Container ID = $ContainerID"
  OStype=$(sudo pct config "$ContainerID" | grep ^ostype | awk '{ print $2 }')
  Name=$(sudo pct config "$ContainerID" | grep ^hostname\\: | awk '{ print $2 }')

  case "$OStype" in

    devuan|debian|ubuntu)
      echo "INFO: Container: $ContainerID, ostype=$OStype, hostname=$Name, updating:"
      sudo pct exec "$ContainerID" -- bash -c "source /etc/default/locale; apt update && apt upgrade -yV"
      echo;;

    centos)
      echo "INFO: Container: $ContainerID, ostype=$OStype, hostname=$Name, updating:"
      sudo pct exec "$ContainerID" -- bash -c "yum -y update"
      echo;;

    *)
      echo "INFO: Container: $ContainerID, ostype=$OStype, hostname=$Name, not updating."
      echo;;
  esac

done
echo "INFO: Finished successfully."
exit
